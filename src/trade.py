from flask import Flask, jsonify, request
from flask_restful import Resource, Api, reqparse
import json
import oandapyV20
import oandapyV20.endpoints.trades as trades
from oandapyV20.exceptions import V20Error, StreamTerminated

def getusercredentials():
    parser = reqparse.RequestParser()    
    parser.add_argument('accountid')
    parser.add_argument('token')

    args = parser.parse_args()

    request_data = request.get_json(force=True)

    token = request_data['token']
    accountid = request_data['accountid']

    client = oandapyV20.API(access_token=token)

    return client, accountid

class ClientTradeList(Resource):
    def get(self):
        client, accountid = getusercredentials()
        try:
            r = trades.TradesList(accountid)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400

class ClientOpenTrade(Resource):
    def get(self):
        client, accountid = getusercredentials()
        try:
            r = trades.OpenTrades(accountid)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400

class ClientTradeDetail(Resource):
    def get(self, tradeid):
        client, accountid = getusercredentials()
        try:
            r = trades.TradeDetails(accountid, tradeID=tradeid)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400

class ClientCloseTrade(Resource):
    def put(self, tradeid):
        client, accountid = getusercredentials()
        try:
            r = trades.TradeClose(accountid, tradeID=tradeid)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400