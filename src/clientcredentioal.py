from flask import Flask, request
from flask_restful import Resource, reqparse
import oandapyV20
import oandapyV20.endpoints.accounts as accounts

class ClientCredential(Resource):
            
    def get():
        parser = reqparse.RequestParser()
        parser.add_argument('accountid')
        parser.add_argument('token')
        args = parser.parse_args()

        request_data = request.get_json(force=True)
        
        token = request_data['token']
        accountid = request_data['accountid']
        
        client = oandapyV20.API(access_token=token)    
        # print(client)
        return client