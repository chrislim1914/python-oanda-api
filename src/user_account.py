from flask import Flask, jsonify, request
from flask_restful import Resource, Api, reqparse
import oandapyV20
import oandapyV20.endpoints.accounts as accounts
from oandapyV20.exceptions import V20Error, StreamTerminated

def getusercredentials():
    parser = reqparse.RequestParser()
    parser.add_argument('accountid')
    parser.add_argument('token')
    args = parser.parse_args()

    request_data = request.get_json(force=True)

    token = request_data['token']
    accountid = request_data['accountid']
    
    client = oandapyV20.API(access_token=token)    

    return client, accountid
    

class UserAccounts(Resource):
    def get(self):        
        client, accountid = getusercredentials()
        try:
            r = accounts.AccountDetails(accountID=accountid)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400

class UserAccountSummary(Resource):
    def get(self):
        client, accountid = getusercredentials()
        try:
            r = accounts.AccountSummary(accountID=accountid)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400

class InstrumentList(Resource):
    def get(self):
        client, accountid = getusercredentials()
        try:
            r = accounts.AccountInstruments(accountID=accountid)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400
