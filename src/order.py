from flask import Flask, jsonify, request
from flask_restful import Resource, Api, reqparse
import json
import oandapyV20
import oandapyV20.endpoints.orders as orders
from oandapyV20.exceptions import V20Error, StreamTerminated



def getusercredentials():
    parser = reqparse.RequestParser()    
    parser.add_argument('accountid')
    parser.add_argument('token')
    parser.add_argument('order')

    args = parser.parse_args()

    request_data = request.get_json(force=True)

    token = request_data['token']
    accountid = request_data['accountid']
    data = request_data['order']

    client = oandapyV20.API(access_token=token)

    return client, accountid, data

class OrderCreate(Resource):
    def post(self):

        # sample request
        # {
        #     "accountid": "101-011-10399412-001",
        #     "token": "d9b133854bed249506d7423b959b34b2-1d294d593b5e759a6053c785605f7c9b",
        #     "order": {
        #         "units": "200",
        #         "instrument": "EUR_USD",
        #         "timeInForce": "FOK",
        #         "type": "MARKET",
        #         "positionFill": "DEFAULT"
        #     }
        # }

        client, accountid, data = getusercredentials()
        orderData = { "order": data }
        try:
            r = orders.OrderCreate(accountid, data=orderData)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400

class OrderClientList(Resource):
    def get(self):

        # sample request
        # {
        #     "accountid": "101-011-10399412-001",
        #     "token": "d9b133854bed249506d7423b959b34b2-1d294d593b5e759a6053c785605f7c9b",
        #     "order": {
        #         "instrument": "EUR_USD",
        #     }
        # }

        client, accountid, data = getusercredentials()
        params = { "params": data }
        try:
            r = orders.OrderList(accountid, params=params)
            rv = jsonify(client.request(r))
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400

class ClientPendingOrder(Resource):
    def get(self):
        client, accountid, data = getusercredentials()
        try:
            r = orders.OrdersPending(accountid)
            rv = jsonify(client.request(r))
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400

class ClientOrderDetail(Resource):
    def get(self, orderid):
        client, accountid, data = getusercredentials()
        try:
            r = orders.OrderDetails(accountid, orderID=orderid)
            rv = jsonify(client.request(r))
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400
