from flask import Flask, jsonify, request
from flask_restful import Resource, Api, reqparse
import oandapyV20
import oandapyV20.endpoints.instruments as instruments
from oandapyV20.exceptions import V20Error, StreamTerminated

# sample request
        # {
        #     "accountid": "101-011-10399412-001",
        #     "token": "d9b133854bed249506d7423b959b34b2-1d294d593b5e759a6053c785605f7c9b",
        #     "candle": "EUR_USD",
        #     "params": {
        #         "granularity": "M1",
        #         "count": 10,
        #         "price": "M"   other data for price B,A BA
        #     }
        # }

def getusercredentials():
    parser = reqparse.RequestParser()
    parser.add_argument('accountid')
    parser.add_argument('token')
    parser.add_argument('candle')
    parser.add_argument('params')
    args = parser.parse_args()

    request_data = request.get_json(force=True)

    token = request_data['token']
    accountid = request_data['accountid']
    candle = request_data['candle']
    params = request_data['params']

    client = oandapyV20.API(access_token=token)

    return client, accountid, candle, params

class InstrumentData(Resource):
    def get(self): 
        client, accountid, candle, params = getusercredentials()
        try:
            r = instruments.InstrumentsCandles(instrument=candle, params=params)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400        

class InstrumentOrder(Resource):
    def get(self):
        client, accountid, candle, params = getusercredentials()

        try:
            r = instruments.InstrumentsOrderBook(instrument=candle, params=params)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400

class InstrumentPosition(Resource):
    def get(self):
        client, accountid, candle, params = getusercredentials()

        try:
            r = instruments.InstrumentsPositionBook(instrument=candle, params=params)
            rv = jsonify(client.request(r), 200)
            return rv
        except StreamTerminated as e:
            return '{}'.format(e), 400
        except V20Error as e:
            return '{}'.format(e), 400