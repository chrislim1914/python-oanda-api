from flask import Flask, jsonify, request
from flask_restful import Resource, Api, reqparse

from src.user_account import UserAccounts, UserAccountSummary, InstrumentList
from src.instrument import InstrumentData, InstrumentOrder, InstrumentPosition
from src.order import OrderCreate, OrderClientList, ClientPendingOrder, ClientOrderDetail
from src.trade import ClientTradeList, ClientOpenTrade, ClientTradeDetail, ClientCloseTrade

app = Flask(__name__)
api = Api(app)


# routes for user
api.add_resource(InstrumentList, '/user/instrumentlist')
api.add_resource(UserAccounts, '/user/details')
api.add_resource(UserAccountSummary, '/user/summary')

# routes for instruments
api.add_resource(InstrumentData, '/instrument')
api.add_resource(InstrumentOrder, '/instrument/order')
api.add_resource(InstrumentPosition, '/instrument/position')

#  routes for order
api.add_resource(OrderCreate, '/order/create')
api.add_resource(OrderClientList, '/order/list')
api.add_resource(ClientPendingOrder, '/order/pending')
api.add_resource(ClientOrderDetail, '/order/<int:orderid>')

# routes for trades
api.add_resource(ClientTradeList, '/trade/all')
api.add_resource(ClientOpenTrade, '/trade/open')
api.add_resource(ClientTradeDetail, '/trade/<int:tradeid>')
api.add_resource(ClientCloseTrade, '/trade/<int:tradeid>')

app.run(port=5000, debug=True)
